#!/usr/bin/env bash

# This script writes the tag Description in the specified file, or in all files
# under the specified directory.
#
# Notes
#
# - $/ makes a new line in a redirection expression;
# - single quotes must be used in Mac/Linux shells around arguments containing a
#   dollar sign;
# - double quotes inside single quotes are for arguments that contain spaces;
# - it may fail to append a comment to a file that already has one if you
#   put the new comment between quotes in the command line, like
#     etcomment-pdf ~/Directory "Comment"
#   Try removing the quotes.
#
# Reference: http://www.sno.phy.queensu.ca/~phil/exiftool/exiftool_pod.html

if (( "$#" == 0 )); then
  echo >&2 \
"Usage:

  etcomment-pdf <file or directory> <comment>

To put a newline in a comment run

  etcomment-pdf <file or directory> $'Line1\nLine2'

or

  etcomment-pdf <file or directory> 'Line1
  Line2'

CAUTION: Escape dumb apostrophes in the comment
when you use ANSI-C quoting ($'...')."
#
# Note: to insert a newline,
#   etcomment-pdf <directory> Line1$/Line2
# works too, but I think the other syntax is more error-proof.
#
  exit 1
fi

exiftool -if '$Description'                      \
             '-Description<$Description; '"$2"'' \
         -execute                                \
         -if 'not $Description'                  \
             -Description="$2"                   \
         -common_args -preserve "$1"
