#!/usr/bin/env python

# This is a workaround for https://trac.ffmpeg.org/ticket/4448
#
# Usage:
#
#   python embed_thumbnail_opus.py AUDIO.opus IMAGE.jpg
#
# https://hydrogenaud.io/index.php/topic,112281.0.html#msg925385

import sys
import base64
from mutagen.oggopus import OggOpus
from mutagen.flac import Picture
from PIL import Image

audio = OggOpus(sys.argv[1])

im = Image.open(sys.argv[2])
width, height = im.size
with open(sys.argv[2], "rb") as c:
    data = c.read()
picture = Picture()
picture.data = data
picture.type = 17
picture.desc = u"Cover Art"
picture.mime = u"image/jpeg"
picture.width = width
picture.height = height
picture.depth = 24
picture_data = picture.write()
encoded_data = base64.b64encode(picture_data)
vcomment_value = encoded_data.decode("ascii")
audio["metadata_block_picture"] = [vcomment_value]

audio.save()
