#!/bin/bash

# https://pastebin.com/j6HF5q8T
# via https://emacsconf.org/2020/talks/30/

for i in {0..255} ; do
    printf "\x1b[48;5;%sm%3d\e[0m " "$i" "$i"
    if (( i == 15 )) || (( i > 15 )) && (( (i-15) % 6 == 0 )); then
        printf "\n";
    fi
done
