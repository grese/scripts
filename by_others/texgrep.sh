#!/usr/bin/env bash

# texgrep - searches for a text pattern contained in files
#   located inside the texmf trees
# usage: texgrep pattern [extension]
# usage examples:
#   texgrep phantomsection sty
#   texgrep \\\\def\\\\phantomsection
# From https://tex.stackexchange.com/questions/4075/grepping-through-an-entire-texmf-tree
# and http://texblog.net/latex-archive/tools/shell-scripts-linux-unix/

if [ $# -eq 0 ]; then
  echo 1>&2 "Usage: texgrep pattern [extension]"
  exit 1
fi

grep -r --line-number --include="*.$2" "$1" $(kpsewhich -expand-path '$TEXMF' | sed 's/:/ /g')

exit 0
