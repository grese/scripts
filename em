#!/bin/sh

# Open an existing Emacs client frame or create a new one.
#
# https://www.gnu.org/software/emacs/manual/html_node/emacs/emacsclient-Options.html

# If there is an Emacs frame lying around the following 'emacsclient' command
# returns t and Grep exits with status 0; if there isn't any Emacs frame in any
# workspace 'emacsclient' doesn't print anything and Grep exits with status 1.
# https://superuser.com/questions/358037/emacsclient-create-a-frame-if-a-frame-does-not-exist/862809#862809
emacsclient --no-wait --eval "(> (length (frame-list)) 1)" \
    | grep --quiet t

if (( "$?" == 0 )); then
    # A frame already exists, raise it and give it focus
    # FIXME: It only works when another window covers the Emacs frame at least
    # partially.
    emacsclient \
        `# Use the existing frame` \
        --reuse-frame \
        `# Focus that frame` \
        --eval '(select-frame-set-input-focus (selected-frame))' \
        `# Return immediately, do not wait for the emacsclient process to` \
        `# complete` \
        `# REVIEW (bug?): emacsclient's man page says: "If combined with ` \
        `# --eval, this option is ignored", but it's not true.` \
        --no-wait
else
    # Create a new frame
    emacsclient \
        `# Start the server if it is not running already` \
        --alternate-editor= \
        `# Without --reuse-frame you get "emacsclient: file name or argument` \
        `# required"` \
        --reuse-frame \
        --frame-parameters='((width . 80) (height . 40))' \
        --no-wait
fi
